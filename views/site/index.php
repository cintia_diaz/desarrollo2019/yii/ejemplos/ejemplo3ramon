<?php

/* @var $this yii\web\View */

$this->title = 'Noticias';
?>
<div class="container">
<div class="row row-flex row-flex-wrap">
  <?php
    $div='<div class="clearfix"></div>';
    foreach($noticias as $k=>$noticia){
        echo $this->render("_noticias",[
            "titulo"=>$noticia->titulo,
            "texto"=>$noticia->texto,
            "reset"=>((($k+1)%3)==0 && $k!=0)?$div:"",
        ]);
    }
  ?>
</div>
</div>

<?= $this->render("_modal",[
    "titulo"=>"Pagina de inicio",
    "texto"=>"Estamos comenzando la aplicacion",
    "boton"=>"Cerrar"
]);?>
